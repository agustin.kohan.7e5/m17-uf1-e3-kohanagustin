using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatePlayer : MonoBehaviour
{

    private SpriteRenderer _sr;
    private int _frames;
    [SerializeField]
    public float FramesPerSecont = 12;
    float _animationFrameRate;
    float _time;

    // Start is called before the first frame update
    void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        _frames = 0;
        _animationFrameRate = 1 / FramesPerSecont;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<Patrol>().Wait == false)
            Animate();
        else
            _sr.sprite = _sprites[2];
    }

    [SerializeField]
    private Sprite[] _sprites;
    private int _contSprite;


    //Time.deltaTime -> Tiempo que dura un frame
    //Un ciclo de animacion tiene que durar 12 ticks
    //Un ciclo dura 1 segundo
    //Cuanto dura 1 frame si la v = 12 frame / 1s
    // t = frames / v ==> 12 / 1 ? 

    int _cont;

    void Animate()
    {
        _time += Time.deltaTime;

        if (_time >= _animationFrameRate)
        {
            _cont++;

            if(_cont == _sprites.Length / FramesPerSecont)

            _frames = 0;
            _sr.sprite = _sprites[_contSprite];
            _contSprite = _contSprite < _sprites.Length - 1 ? _contSprite + 1 : 0;
            _time = 0;
            _cont = 0;
        }
        _frames++;
    }
}
