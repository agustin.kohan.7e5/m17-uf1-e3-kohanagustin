using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EventStartGame : MonoBehaviour
{
    [SerializeField] private Button _StartButton = null;
    string _playerName;
    [SerializeField]
    string _nextScene;
    // Start is called before the first frame update
    void Start()
    {
        _StartButton.onClick.AddListener(() => { StartUIPanelGame(); });
    }

    bool isValidName;
    public void StartUIPanelGame()
    {
        isValidName = true;
        _playerName = GameObject.Find("PlayerName").GetComponent<PlayerName>().NamePlayer;
        if (_playerName == "") isValidName = false;
        foreach (char character in _playerName)
            if (character == ' ') isValidName = false;
        
        if (isValidName)
            SceneManager.LoadScene(_nextScene);
    }
}
