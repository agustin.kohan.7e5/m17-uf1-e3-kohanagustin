using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class FollowThePlayer : MonoBehaviour
{
    [SerializeField]
    GameObject _player;

    [SerializeField]
    Camera MainCamera;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Text>().text = _player.GetComponent<PlayerData>().PlayerName + " / " + _player.GetComponent<PlayerData>().PlayerType;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = MainCamera.WorldToScreenPoint(_player.transform.position + new Vector3(1,3,0));
    }
}
