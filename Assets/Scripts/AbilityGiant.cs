using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityGiant : MonoBehaviour
{
    enum abilityCooldown
    {
        InProcess,
        ActiveTrue,
        ActiveFalse
    }

    float GrowingSpeed = 0.1f;
    int GrowingVariable = 2;

    float _maxHeight;
    float _minHeight;

    abilityCooldown _abilityCooldownState = abilityCooldown.ActiveFalse;
    
    public float _cooldown = 5f;
    float _cooldownTimer = 0f;
    float _abilityTimer = 0f;



    [SerializeField]
    Button _abilityButton;
    // Start is called before the first frame update
    void Start()
    {
        _minHeight = gameObject.GetComponent<PlayerData>().Height;
        _maxHeight = gameObject.GetComponent<PlayerData>().Height * GrowingVariable;
        _abilityButton.onClick.AddListener(() => { CheckState(); });
    }

    private void CheckState()
    {
        if (_abilityCooldownState == abilityCooldown.ActiveFalse)
            ChangeState();
    }

    private void ChangeState()
    {
        Debug.Log("Enter ChangeState");
        gameObject.GetComponent<PlayerData>().PlayerState = PlayerData.playerState.Giant;
        _abilityCooldownState = abilityCooldown.InProcess;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(_abilityCooldownState);
        
        if (Input.GetKey(KeyCode.Space) || _abilityCooldownState == abilityCooldown.InProcess)
        {
            //Debug.Log("Enter in keycode IF statement");
            if (_abilityCooldownState == abilityCooldown.ActiveFalse)
                ChangeState();
                
            else
                ChangeSize();
        }
        else if(_abilityCooldownState == abilityCooldown.ActiveTrue)
        {
            Debug.Log(_cooldownTimer);
            _cooldownTimer += Time.deltaTime;
        }
        

        if (_cooldownTimer >= _cooldown)
        {
            Debug.Log("Ready to use");
            _cooldownTimer = 0;
            _abilityCooldownState = abilityCooldown.ActiveFalse;
            gameObject.GetComponent<PlayerData>().PlayerState = PlayerData.playerState.Normal;
        }
        
    }

    void ChangeSize()
    {
        //Debug.Log("Enter ChangeSize");
        //Debug.Log(gameObject.GetComponent<PlayerData>().PlayerState);
        if (gameObject.GetComponent<PlayerData>().PlayerState == PlayerData.playerState.Giant)
        {
            if (gameObject.GetComponent<PlayerData>().Height <= _maxHeight)
            {
                transform.localScale = new Vector3(transform.localScale.x + GrowingSpeed, transform.localScale.y + GrowingSpeed);
                gameObject.GetComponent<PlayerData>().Height += GrowingSpeed;
            }

            WhaitAbiltyDuration();
        }
        else
        {
            if (gameObject.GetComponent<PlayerData>().Height >= _minHeight)
            {
                //Debug.Log("Chiquito");
                transform.localScale = new Vector3(transform.localScale.x - GrowingSpeed, transform.localScale.y - GrowingSpeed);
                gameObject.GetComponent<PlayerData>().Height -= GrowingSpeed;
            }
            else _abilityCooldownState = abilityCooldown.ActiveTrue;
        }
    }

    void WhaitAbiltyDuration()
    {
        _abilityTimer += Time.deltaTime;
        Debug.Log(_abilityTimer);
        if (_abilityTimer >= 5f)
        {
            _abilityTimer = 0f;
            gameObject.GetComponent<PlayerData>().PlayerState = PlayerData.playerState.Normal;
        }
    }
}
