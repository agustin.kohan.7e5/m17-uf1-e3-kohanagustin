using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{

    enum facing
    {
        right,
        left
    }

    public bool Wait = false;

    facing _facing;
    float _walkingDistance;
    float _startPosition;
    float _speed;
    SpriteRenderer _spriteRenderer;
    float _walkDirection = -1;
    float _timer = 0f;


    // Start is called before the first frame update
    void Start()
    {
        _speed = gameObject.GetComponent<PlayerData>().Speed / gameObject.GetComponent<PlayerData>().Weight;
        Debug.Log(_speed);
        _walkingDistance = gameObject.GetComponent<PlayerData>().WalkingDistance;
        Debug.Log(_walkingDistance);
        _startPosition = transform.position.x;
        Debug.Log(_startPosition);
        Debug.Log(_startPosition + _walkingDistance);
        _facing = facing.left;

        if (_facing == facing.right) _walkDirection = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (_facing == facing.right) _walkDirection = 1;  
        else _walkDirection = -1;
        if(!Wait)
            Walk();

        //|| transform.position.x <= _startPosition.position.x
        if (transform.position.x <= _startPosition + _walkingDistance || transform.position.x >= _startPosition)
        {
            Wait = true;
           _timer += Time.deltaTime;
            if(_timer >= 2)
            {
                _timer = 0;
                Wait = false;
                Turn();
            }
        }
    }

    void Walk()
    {
        transform.position += new Vector3(_walkDirection * _speed * Time.deltaTime, transform.position.y);
    }

    void Turn()
    {
        if(_facing == facing.left)
        {
            transform.Rotate(0, 180, 0);
            _facing = facing.right;
        }

        else
        {
            transform.Rotate(0, -180, 0);
            _facing = facing.left;
        }      
    }
}
