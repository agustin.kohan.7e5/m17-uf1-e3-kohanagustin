using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveName : MonoBehaviour
{
    [SerializeField] private InputField _playerNameField = null;
    [SerializeField] private GameObject _playerName;

    public void SavePlayerName()
    {
        _playerName.GetComponent<PlayerName>().NamePlayer = _playerNameField.text;
    }
}
