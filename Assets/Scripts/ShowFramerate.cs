using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowFramerate : MonoBehaviour
{
    [SerializeField]
    Text _framerateText;

    
    // Start is called before the first frame update
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        _framerateText.text = "FPS: " + (1f / Time.deltaTime).ToString();
    }
}
